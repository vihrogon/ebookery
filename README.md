# eBookery

## Description

An online library of books I like.

Features:

- local caching of books
- remember last book and line read
- persistent settings

## Development Setup

- install git `https://git-scm.com/`
- clone repository `git clone https://gitlab.com/vihrogon/ebookery.git`
- install Deno `https://docs.deno.com/runtime/manual#install-deno`
- run command `deno run -A --watch main.js`

## TODO

- [x] logs
- [ ] better logs
- [ ] bug fixes
- [ ] new features
- [ ] more books

## MVP - DONE

- [x] prepare book format (json)
- [x] prepare books
- [x] FE list available books
- [x] load selected book text
- [x] observe last line read
- [x] on load restore last line read
- [x] store settings changes
- [x] on load use stored settings
