import { Router } from "./Router.js";

const router = new Router();

router.static();
router.get("/", ({req}) => {
    return router.route(new Request(req.url + 'static/index.html', { headers: req.headers }))
});

Deno.serve({
    port: Deno.env.get('PORT'),
    hostname: Deno.env.get('HOST'),
},async (req, remote) => {
    const start = Date.now();
    const res = await router.route(req);
    const end = Date.now();

    Deno.writeTextFileSync('./calls.log', `${new Date(start).toJSON()} - ${end - start}ms - ${res.status ? res.status : 200} - ${JSON.stringify(remote.remoteAddr)} - ${req.method} - ${req.url}\n`, {
        create: true,
        append: true,
    });

    return res;
});
